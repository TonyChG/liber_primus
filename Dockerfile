FROM docker.io/library/python:3.9

WORKDIR /work

ADD requirements.txt requirements.txt

RUN python3.9 -m pip install --user -U pip \
		&& python3.9 -m pip install --user -r requirements.txt

ENTRYPOINT ["/usr/bin/python3.9", "solve_liberprimus.py"]
