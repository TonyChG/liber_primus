# coding: utf8
# vi: ft=python expandtab ts=4 sts=4 sw=4

import re
import time
import itertools
from typing import Iterable

import pickle
import numpy as np
import matplotlib.pyplot as plt
from rich.console import Console


start_at = time.time()
console = Console()
liber_primus_substitutions = {
    re.compile(r"(ING|NG)"): "ñ",
    re.compile(r"(IA|IO)"): "ʝ",
    re.compile(r"(OE)"): "œ",
    re.compile(r"(EO)"): "ξ",
    re.compile(r"(EA)"): "ɇ",
    re.compile(r"(AE)"): "ǽ",
    re.compile(r"(TH)"): "ʈ",
    re.compile(r"(Z)"): "S",
    re.compile(r"(K)"): "C",
}
GP = [
    ["0x16A0", "F", "", "2"],
    ["0x16A2", "V", "", "3"],  # U V
    ["0x16A6", "ʈ", "", "5"],
    ["0x16A9", "O", "", "7"],
    ["0x16B1", "R", "", "11"],
    ["0x16B3", "C", "", "13"],  # C K
    ["0x16B7", "G", "", "17"],
    ["0x16B9", "W", "", "19"],
    ["0x16BB", "H", "", "23"],
    ["0x16BE", "N", "", "29"],
    ["0x16C1", "I", "", "31"],
    ["0x16C4", "J", "", "37"],  # used to be 0x16C2 due to error.
    ["0x16C7", "ξ", "", "41"],
    ["0x16C8", "P", "", "43"],
    ["0x16C9", "X", "", "47"],
    ["0x16CB", "S", "", "53"],  # S Z
    ["0x16CF", "T", "", "59"],
    ["0x16D2", "B", "", "61"],
    ["0x16D6", "E", "", "67"],
    ["0x16D7", "M", "", "71"],
    ["0x16DA", "L", "", "73"],
    ["0x16DD", "ñ", "", "79"],  # NG ING
    ["0x16DF", "œ", "", "83"],
    ["0x16DE", "D", "", "89"],
    ["0x16AA", "A", "", "97"],
    ["0x16AB", "ǽ", "", "101"],
    ["0x16A3", "Y", "", "103"],
    ["0x16E1", "ʝ", "", "107"],  # IA IO
    ["0x16E0", "ɇ", "", "109"],
]
regex_punctuation = re.compile(r"[,!'\(\)\/\:\;\?’&]")
regex_non_letters = re.compile(
    r"([0-9\|\[\]\.\"\”\@\-\“\—\•\‘\•\■\ɇ#%\$\*]|\\)"
)


def open_filename(filename: str) -> Iterable[str]:
    try:
        with open(filename, "r", encoding="utf8") as file_object:
            yield from file_object
    except OSError as error:
        print(error)


def read_dictionnary(filename: str) -> Iterable[str]:
    for line in open_filename(filename):
        word = line[:-1].upper()
        word = regex_non_letters.sub("", word)
        word = regex_punctuation.sub(" ", word)
        if len(word) > 0:
            yield word.strip()


def read_book(filename: str) -> Iterable[str]:
    for line in open_filename(filename):
        # line = re.sub(r"^ [A-Z].+", "", line)
        for word in line[:-1].strip().upper().split(" "):
            word = regex_non_letters.sub("", word)
            word = regex_punctuation.sub(" ", word)
            if len(word) > 0:
                yield word.strip()


def substitute(word: str, pattern: re.Pattern, replacement: str) -> str:
    if pattern.search(word) is not None:
        return pattern.sub(replacement, word)
    return word


def substitute_all(word: str, substitutions: dict[re.Pattern, str]) -> str:
    word_result = word
    for pattern, replacement in substitutions.items():
        word_result = substitute(word_result, pattern, replacement)
    # if word != word_result:
    #     print("Substitute : %s -> %s" % (word, word_result))
    return word_result


def get_index_of(char, alphabet):
    for i in range(len(alphabet)):
        if char == alphabet[i]:
            return i
    return 0


def write_bigrams(bigrams, destination):
    bigrams.tofile(destination)


def write_bigrams_image(alphabet, bigrams, destination):
    p2D = (
        bigrams.astype("float") / np.tile(sum(bigrams.T), (len(alphabet), 1)).T
    )
    p2D[np.isnan(p2D)] = 0

    alpha = 0.33
    p2Da = p2D ** alpha
    plt.figure(figsize=(8, 8))
    plt.imshow(p2Da, interpolation="nearest", cmap="inferno")
    plt.axis("off")

    for ip, i in enumerate(alphabet):
        plt.text(
            -1, ip, i, horizontalalignment="center", verticalalignment="center"
        )
        plt.text(
            ip, -1, i, horizontalalignment="center", verticalalignment="center"
        )
    plt.savefig(destination)
    return p2Da


def create_bigrams(content: str):
    alphabet = tuple(sorted(set(content)))
    count = np.zeros(len(alphabet) ** 2)

    for c in content:
        count[get_index_of(c, alphabet)] += 1

    for i in range(512):
        if count[i] > 0:
            console.print(str(i) + " " + alphabet[i] + " " + str(count[i]))

    bigrams = np.zeros((len(alphabet), len(alphabet)), dtype="int32")
    i = 0
    for c in content:
        j = 0 if c == " " else get_index_of(c, alphabet)
        bigrams[i, j] += 1
        i = j
    return alphabet, bigrams


def calculate_logp(bigrams_filename, alphabet):
    bigrams = np.fromfile(bigrams_filename, dtype="int32").reshape(
        len(alphabet), len(alphabet)
    )
    p = bigrams.astype("float") / np.tile(sum(bigrams.T), (len(alphabet), 1)).T
    p[np.isnan(p)] = 0
    EPSILON = 1e-6
    return np.log(p + EPSILON)


def likelihood(s, alphabet, logp):
    res = 0
    c1 = s[0]
    for c2 in s[1:]:
        i = get_index_of(c1, alphabet)
        j = get_index_of(c2, alphabet)
        res += logp[i, j]
        c1 = c2

    return res / len(s)


def apply_code(s, code, alphabet):
    res = ""
    for c in s:
        res += alphabet[code[get_index_of(c, alphabet)]]
    return res


def guess_from_frequency(freq_text, ciphered_text, alphabet, logp):
    ref_freq = frequency_order(freq_text, alphabet)
    obs_freq = frequency_order(ciphered_text, alphabet)

    freq_code = [0] + list(range(1, len(alphabet)))

    for i in range(1, len(alphabet)):
        pos = obs_freq.index(i)
        freq_code[i] = ref_freq[pos]

    cur_code = freq_code.copy()
    cur_trad = apply_code(ciphered_text, cur_code, alphabet)
    cur_like = likelihood(cur_trad, alphabet, logp)

    # Best found so far
    best_code = cur_code.copy()
    best_like = cur_like
    best_trad = cur_trad

    console.print(f"{best_trad}", justify="full")
    console.print(f"N={str(0)} L={best_like} best_code={best_code}")
    return cur_code, cur_like, best_like


def frequency_order(s, alphabet):
    res = np.zeros(len(alphabet) - 1)
    for i in range(len(alphabet) - 1):
        res[i] = s.count(alphabet[i])
    return list(1 + np.argsort(res)[::-1])


def gp_to_dict():
    return dict(map(lambda x: (x[0].lower(), x[1:-1]), GP))


def split_by_sep(data, sep=" "):
    for item in data.split(sep):
        yield item


def read(page_number=0, offset=1):
    with open(
        "./data/liber-primus__transcription--master.txt",
        encoding="utf8"
        # "./data/liber-primus__transcription--unsolved.txt", encoding="utf8",
    ) as file_object:
        data = file_object.read().replace("\n", "")

        for page in tuple(split_by_sep(data, "%"))[
            page_number : page_number + offset
        ]:
            for word in re.split(r"[&§$\.\-]", re.sub(r"/", "", page)):
                if len(word) > 0:
                    yield word.strip()


def get_ciphered_text(page_number=0, offset=1):
    runes_to_letter = gp_to_dict()
    words = []

    for word in read(page_number, offset=offset):
        word_transcripted = ""

        for rune in word:
            letters = runes_to_letter.get(str(hex(ord(rune))))
            if letters is not None:
                if letters[-1] == "":
                    word_transcripted += letters[0]
                else:
                    word_transcripted += "(%s)" % "/".join(letters)
            else:
                word_transcripted += rune
        words.append(word_transcripted)
    return " ".join(words)


def main_loop(
    cur_code,
    cur_like,
    best_like,
    alphabet,
    logp,
    ciphered_text,
    min_iter=2000,
    max_iter=100000,
    threshold=-2.05,
):
    # Now the main loop

    # MIN_ITER = 2000
    # MAX_ITER = 100000
    # THRESHOLD = -2.05
    ALPHA = 1

    for k in range(max_iter):
        # Build a tentative move
        i = np.random.randint(1, len(alphabet))
        j = np.random.randint(1, len(alphabet))
        tt_code = permute_code(cur_code, i, j)

        tt_trad = apply_code(ciphered_text, tt_code, alphabet)
        tt_like = likelihood(tt_trad, alphabet, logp)

        # Test whether move should be accepted
        x = np.random.rand()
        p = np.exp(ALPHA * (tt_like - cur_like) * len(ciphered_text))

        if x < p:
            cur_code = tt_code.copy()
            cur_trad = tt_trad
            cur_like = tt_like
            # print("ACCEPT")

            if cur_like > best_like:
                best_code = cur_code.copy()
                best_like = cur_like
                best_trad = cur_trad
                console.print(f"{best_trad}")
                console.print(f">> k={str(k)} L={best_like}")

        if k > min_iter and best_like > threshold:
            break
    return best_trad, best_like, best_code


def count_correct_words(s, dictionnary_words):
    cnt = 0
    word_list = s.split(" ")
    for w in word_list:
        if w in dictionnary_words:
            cnt += 1
    return cnt, len(word_list)


def score_correct_words(s, dictionnary_words):
    res = 0
    tot = 0
    word_list = s.split(" ")
    for w in word_list:
        if w in dictionnary_words:
            res += len(w)
        tot += len(w)
    return res / tot


def second_phase(
    dictionnary_data,
    best_trad,
    best_like,
    best_code,
    ciphered_text,
    alphabet,
    logp,
):
    console.print(
        ">>>>>>>>>>>>>>>>> Enter second phase <<<<<<<<<<<<<<<<<<<<<<",
        style="bold white on black",
    )

    with open(dictionnary_data, "rb") as filehandle:
        dictionnary_words = pickle.load(filehandle)

    cnt, total = count_correct_words(best_trad, dictionnary_words)
    word_score = score_correct_words(best_trad, dictionnary_words)

    console.print(
        "Mots OK " + str(cnt) + "/" + str(total) + " score=" + str(word_score)
    )

    GAMMA = 4.0
    best_score = GAMMA * word_score + best_like

    cur_code = best_code
    cur_score = best_score
    cur_trad = best_trad

    NITER2 = 2000
    temperature = 0.05
    rho = 0.999

    for k in range(NITER2):
        # Build a tentative move and compute score
        i = np.random.randint(1, len(alphabet))
        j = np.random.randint(1, len(alphabet))
        tt_code = permute_code(cur_code, i, j)
        tt_trad = apply_code(ciphered_text, tt_code, alphabet)
        tt_word_score = score_correct_words(tt_trad, dictionnary_words)
        tt_like = likelihood(tt_trad, alphabet, logp)
        tt_score = GAMMA * tt_word_score + tt_like

        # Test whether move should be accepted
        x = np.random.rand()
        p = np.exp((tt_score - cur_score) / temperature)
        temperature = temperature * rho

        if x < p:
            cur_code = tt_code.copy()
            cur_trad = tt_trad
            cur_score = tt_score

            if cur_score > best_score:
                best_code = cur_code
                best_score = cur_score
                best_trad = cur_trad
                console.print(tt_trad)
                console.print(">> W={0:.2f}".format(tt_word_score))
    return best_trad, best_score


def permute_code(code, i, j):
    newcode = code.copy()
    newcode[j] = code[i]
    newcode[i] = code[j]
    return newcode


def create_dictionnary(alphabet, dictionnary, destination):
    words = list(alphabet) + dictionnary.split(" ")
    pickle.dump(words, open(destination, "wb"))


def write_results(page_number, score, ciphered_text, trad):
    end_at = time.time()
    with open(
        f"./dest/{page_number}.txt", "a+", encoding="utf8"
    ) as file_object:
        file_object.write(
            "%s\n"
            % ",".join(
                [
                    str(start_at),
                    str(end_at),
                    str(score),
                    ciphered_text,
                    trad,
                ]
            )
        )


def mcmc(book, ciphered_text, dictionnary_data, page_number):
    alphabet, _ = create_bigrams(book)
    logp = calculate_logp("./data/bigrams.english.dat", alphabet)
    cur_code, cur_like, best_like = guess_from_frequency(
        book, ciphered_text, alphabet=alphabet, logp=logp
    )
    best_trad, best_like, best_code = main_loop(
        cur_code,
        cur_like,
        best_like,
        alphabet,
        logp,
        ciphered_text,
    )
    console.print(f"Best traduction : {best_trad}")
    trad, score = second_phase(
        dictionnary_data,
        best_trad,
        best_like,
        best_code,
        ciphered_text,
        alphabet,
        logp,
    )
    console.print(f"PROUUUUUUUUUUUT -> {trad}")
    write_results(page_number, score, ciphered_text, trad)


def main():
    CREATE_DICTIONNARY = True
    CREATE_BIGRAMS = True
    PAGE_NUMBER = 0
    OFFSET = 10
    ENGLISH_DICTIONNARY = "./data/words.txt"

    dictionnary = " ".join(
        map(
            lambda x: substitute_all(x, liber_primus_substitutions),
            read_dictionnary(ENGLISH_DICTIONNARY),
        )
    )
    book = " ".join(
        map(
            lambda x: substitute_all(x, liber_primus_substitutions),
            itertools.chain(
                read_book("./data/hamlet.txt"),
                read_book("./data/Book 1 - The Philosopher's Stone.txt"),
            ),
        )
    )
    if CREATE_DICTIONNARY:
        with open(
            "./data/current_dictionnary.txt", "r", encoding="utf8"
        ) as file_object:
            dictionnary = file_object.read()
            alphabet = tuple(set(sorted(book)))
            create_dictionnary(
                alphabet, dictionnary, "./data/dictionnary.english.dat"
            )
    ciphered_text = get_ciphered_text(page_number=PAGE_NUMBER, offset=OFFSET)
    console.print(ciphered_text, style="bold green on black")
    if CREATE_BIGRAMS:
        alphabet, bigrams = create_bigrams(book)
        write_bigrams(bigrams, "./data/bigrams.english.dat")
        write_bigrams_image(alphabet, bigrams, "./data/bigrams.english.png")
    mcmc(book, ciphered_text, "./data/dictionnary.english.dat", PAGE_NUMBER)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
